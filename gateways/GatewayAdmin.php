<?php

class GatewayAdmin
{
    private $con;

    public function __construct($con){
        $this->con = $con;
    }

    public function addAdmin($admin)
    {
        $query = "insert into admin(username,password) values (:username,:password);"; 
        $this->con->executeQuery($query, array(':username' => array($admin->getUsername(), PDO::PARAM_STR),
                                         ':password' => array(password_hash($admin->getPassword(), PASSWORD_DEFAULT), PDO::PARAM_STR)
                                        )
                          );
    }


    public function getCredential($login)
    {
        $query = "SELECT password FROM admin WHERE username = :login;";
        $this->con->executeQuery($query, array(':login' => array($login, PDO::PARAM_STR)));
        $results=$this->con->getResults();
        if($results == NULL){
            return false;
        }
        return $results[0]['password'];
    }

} 

?>
