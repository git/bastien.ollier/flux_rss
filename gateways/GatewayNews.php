<?php

class GatewayNews
{
    private $con;

    public function __construct($con){
        $this->con = $con;
    }

    public function addNews($news)
    {
        $query = "insert into news(title,description,pubdate,link) values (:title,:description,:pubdate,:link);"; 
        $this->con->executeQuery($query, array(':title' => array($news->getTitle(), PDO::PARAM_STR),
                                      	       ':description' => array($news->getDescription(), PDO::PARAM_STR),
                                               ':pubdate' => array(date("Y-m-d H:i:s", strtotime($news->getPubdate())), PDO::PARAM_STR),
                                               ':link' => array($news->getLink(), PDO::PARAM_STR)
                                        )
                          );
    }

    public function getNews($page,$nbArticlePage)
    {
        $query = "SELECT * FROM news order by pubdate desc LIMIT :debut,:fin;";
        $this->con->executeQuery($query, array(':debut' => array($nbArticlePage * ($page-1),PDO::PARAM_INT),
                                                ':fin'  => array($nbArticlePage,PDO::PARAM_INT)
        ));
        $listeNews = array();
        $results=$this->con->getResults();
        Foreach ($results as $article){
            $listeNews[] = new News($article["title"],$article['description'],$article['pubdate'],$article['link']);
        }
        return $listeNews;
    }

    public function  getNbNews(){
        $query = "SELECT count(*) FROM news;";
        $this->con->executeQuery($query, array());
        return $this->con->getResults()[0]["count(*)"];
    }
} 

?>
