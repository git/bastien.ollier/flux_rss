<?php
class GatewaySources
{
    private $con;

    public function __construct($con){
        $this->con = $con;
    }

    public function addSources($sources)
    {
        $query = "insert into sources(link) values (:link);"; 
        $this->con->executeQuery($query, array(':link' => array($sources->getLink(), PDO::PARAM_STR)
                                              )
                                ); 
    }

    public function deleteSources($link)
    {
        $query = "delete from sources where link = :link;";
        $this->con->executeQuery($query, array(':link' => array($link, PDO::PARAM_STR)
                                              )
                                ); 
    }

    public function getSources()
    {
        $query = "SELECT * FROM sources order by id desc";
        $this->con->executeQuery($query, array());
        $results=$this->con->getResults();
        $listeSources = array();
        Foreach ($results as $source){
            $listeSources[] = new Sources($source["link"]);
        }
        return $listeSources;
    }
} 

?>
