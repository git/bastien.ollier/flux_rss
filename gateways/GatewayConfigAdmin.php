<?php

class GatewayConfigAdmin
{
    private $con;

    public function __construct($con){
        $this->con = $con;
    }

    public function getConfigAdmin($idConfig)
    {
        $query = "SELECT * FROM configadmin WHERE :idConfig;";
        $this->con->executeQuery($query, array(':idConfig' => array($idConfig, PDO::PARAM_INT)));
        $results=$this->con->getResults();
        return $results[0]["value"];
    }

    public function updateConfigAdmin($idConfig,$value)
    {
        $query = "UPDATE configadmin SET value = :value WHERE idconfig = :idConfig;";
        $this->con->executeQuery($query, array(':value' => array($value,PDO::PARAM_STR),
                                                ':idConfig'  => array($idConfig,PDO::PARAM_STR)));
    }
} 

?>
