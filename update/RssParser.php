<?php
class RssParser
{   
    public function getResultFlux($url)
    {
        ini_set('display_errors', 1);
	ini_set('user_agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1');
	
	$listeArticles = array();
	
	if (!($x = simplexml_load_file($url)))
            return;

        foreach ($x->channel->item as $item)
        {
            $title = $item->title;
            $description = $item->description;
            $pubdate = $item->pubDate;
            $link = $item->link;
            $listeArticles[] = new News($title,$description,$pubdate,$link);
        }
        
        return $listeArticles;
    }
}
?>
