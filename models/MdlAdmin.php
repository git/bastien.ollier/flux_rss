<?php

class MdlAdmin
{
    public function connection($login, $mdp){
        global $dsn,$user,$pass;
        $gatewayAdmin = new GatewayAdmin(new Connection($dsn,$user,$pass));
        $pass = $gatewayAdmin->getCredential($login);
        if($pass!=false && password_verify($mdp,$pass)){
            $_SESSION['role']='admin';
            $_SESSION['login']=$login;
            return new Admin($login,'admin');
        }
        else{
            return NULL;
        }
    }

    public function deconnexion(){
        session_unset();
        session_destroy();
        $_SESSION = array();
    }

    public function isAdmin(){
        if(isset($_SESSION['login']) && isset($_SESSION['role'])) {
            $login = /*Nettoyer::nettoyer_string(*/$_SESSION['login']/*)*/;
            $role =  /*Nettoyer::nettoyer_string(*/$_SESSION['role']/*)*/;
            return new Admin($login,$role);
        }
        else{
            return null;
        }
    }
}