<?php

class News
{
    private string $title;
    private string $description;
    private string $pubdate;
    private string $link;

    public function __construct(string $title, string $description, string $pubdate, string $link)
    {
        $this->title = $title;
        $this->description = $description;
        $this->pubdate = $pubdate;
        $this->link = $link;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPubdate()
    {
        return $this->pubdate;
    }

    public function getLink() 
    {
        return $this->link;
    }
}
?>
