<?php

class ControllerAdmin {

    function __construct() {
        global $dsn,$user,$pass,$vues;
        session_start();
        try
        {
            $action = "";
            if (isset($_REQUEST['action'])) {
                $action = $_REQUEST['action'];
            }

            switch($action) {
                case "goToAdminConnexion":
                    require($vues['adminConnexion']);
                    break;
                case "goToAdministration":
                    $gatewaySources = new GatewaySources(new Connection($dsn,$user,$pass));
                    $listeSources = $gatewaySources->getSources();
                    $gate = new GatewayConfigAdmin(new Connection($dsn, $user, $pass));
                    $nbArticlePages = $gate->getConfigAdmin(1);
                    require($vues['administration']);
                    break;
                case "refreshData":
                    Appel::refresh();
                    header("location: index.php?action=goToAdministration");
                    break;
                case "verifValidation":
                    $this->validationConnexion();
                    break;
                case "ajoutSource":
                    $this->ajoutSource();
                    break;
                case "supprimerSource":
                    $this->supprimerSource();
                    break;
                case "setNbArticlesParPage":
                    $this->setNbArticlesParPage();
                    break;
                case "quitterAdmin":
                    $mdlAdmin = new MdlAdmin();
                    $mdlAdmin->deconnexion();
                    header("location: index.php");
                    break;
                default:
                     break;
            }
        }
        catch (PDOException $e)
        {
         // $dataVueEreur[] = "Erreur inattendue!!! ";
         // require(__DIR__.'/../vues/erreur.php');
        }
        catch (Exception $e2)
        {
         // $dataVueEreur[] = "Erreur inattendue!!! ";
         // require ($rep.$vues['erreur']);
        }
    }

    function ajoutSource() {
        global $dsn,$user,$pass,$vues;
        $error =[];
        $link = $_REQUEST["linkSource"];
        if(isset($_REQUEST["linkSource"]) && Validation::url_form($link,$error)) {
            $gate = new GatewaySources(new Connection($dsn,$user,$pass));
            $source = new Sources($_REQUEST["linkSource"]);
            $gate->addSources($source);
        }
        header("location: index.php?action=goToAdministration");
    }

    function validationConnexion() {
        global $vues;
        $validation = new Validation();
        $error =[];
        $validation->val_form($_POST['name'],$_POST['password'],$error);
        Foreach ($error as $key) {
            print($key);
        }

        if (empty($error)) {
            $MdlAdmin = new MdlAdmin();
            $validation = $MdlAdmin->connection($_POST['name'],$_POST['password']);
            if (!empty($validation)) {
                header("location: index.php?action=goToAdministration");
            }
            else{
                header("location: index.php?action=goToAdminConnexion");
            }
        }
        else{
            header("location: index.php?action=goToAdminConnexion");
        }
    }

    function supprimerSource() {
        global $dsn,$user,$pass,$vues;
        if(isset($_REQUEST["source"])) {
            $gate = new GatewaySources(new Connection($dsn, $user, $pass));
            $gate->deleteSources($_REQUEST["source"]);
        }
        header("location: index.php?action=goToAdministration");
    }

    function setNbArticlesParPage(){
        global $dsn,$user,$pass,$vues;
        if(isset($_REQUEST["nbArticle"])) {
            $gate = new GatewayConfigAdmin(new Connection($dsn, $user, $pass));
            $gate->updateConfigAdmin(1,$_REQUEST["nbArticle"]);
        }
        header("location: index.php?action=goToAdministration");
    }
}
?>
