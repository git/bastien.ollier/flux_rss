<?php

class FrontController{
    function __construct()
    {
        global $dsn,$user,$pass,$vues;
        $listeAction_Admin = array('goToAdminConnexion','verifValidation', 'ajoutSource','supprimerSource','setNbArticlesParPage','goToAdministration','refreshData','quitterAdmin');
        Try{
            $mdlAdmin = new MdlAdmin();
            $admin = $mdlAdmin->isAdmin();
            $action = "";
            if (isset($_REQUEST['action'])) {
                $action = $_REQUEST['action'];
            }
            if(in_array($action,$listeAction_Admin)){
                if($admin == NULL){
                    new ControllerAdmin();
                }
                else{
                    header("Location:".$vues["adminConnexion"]);
                }
            }
            else{
                $gatewayNews = new GatewayNews(new Connection($dsn,$user,$pass));
                $page = 1;
                if(isset($_GET['page'])){
                    $page = $_GET['page'];
                }
                $gate = new GatewayConfigAdmin(new Connection($dsn, $user, $pass));
                $nbArticlePages = $gate->getConfigAdmin(1);
                $listeNews = $gatewayNews->getNews($page,$nbArticlePages);
                $pageAvant = $page - 1;
                $pageApres = $page + 1;


                $pageMAX = $gatewayNews->getNbNews()/$nbArticlePages + 1;
                require($vues['listeNews']);
            }
        }
        Catch(Exception $e){
            header("Location:".$vues["erreur"]);
        }
    }
}

