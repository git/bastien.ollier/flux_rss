<?php

    class Validation {

        static function val_action($action) 
        {
            if (!isset($action)) 
            {
                throw new Exception('pas d\'action');
            }
        }

        static function val_form(string &$name, string &$password, array &$dVueEreur) 
        {

                if (!isset($name)||$name=="") {
                    $dVueEreur[] = "pas de nom";
                    $name="";
                }

                if ($name != filter_var($name, FILTER_SANITIZE_STRING))
                {
                    $dVueEreur[] = "testative d'injection de code (attaque sécurité)";
                    $nom="";
                }

                if (!isset($password)||$password=="") {
                    $dVueEreur[] = "pas de mot de passe ";
                    $password="";
                }

                if ($password != filter_var($password, FILTER_SANITIZE_STRING))
                {
                    $dVueEreur[] = "testative d'injection de code (attaque sécurité)";
                    $password="";

                }

        }

        static function url_form(string $url,array &$dVueEreur)
        {
                if (!isset($url)||$url=="") {
                    $dVueEreur[] = "pas d'url";
                    $url="";
                    return false;
                }

                if (!filter_var($url, FILTER_VALIDATE_URL))
                {
                    $dVueEreur[] = "url invalide";
                    $url="";
                    return false;
                }
                return true;

        }

    }
?>