<!doctype html>
<link href="css/administration.css" rel="stylesheet" type="text/css">
<html>
    <div id="page">
        <div id="pageRight">
            <div id="listSources">
                <div>
                    <?php
                    foreach ($listeSources as $n){
                        echo '<form action="index.php?action=supprimerSource&source=' . $n->getlink() . '" method="POST">';
                        echo "<div class=source>";
                        echo "<div>";
                        echo "<p id=link>";
                        echo $n->getlink();
                        echo "</p>";
                        echo "</div>";
                        echo "<div>";
                        echo '<input type="submit" value="x">';
                        echo "</div>";
                        echo "</div>";
                        echo "</form>";
                    }
                    ?>
                </div>
                <div id="listSourceBottom">
                    <form action="index.php?action=ajoutSource" method="POST">
                        <input type="text" name="linkSource" placeholder="source" />
                        <input type="submit" value="add source">
                    </form>
                </div>
            </div>
            <div class="bouton">
                <form action="index.php">
                    <input type="hidden" name="action" value="refreshData">
                    <input type="submit" class="buttonClick" value="refresh">
                </form>
            </div>
        </div>
        <div id="pageLeft">
            <div class="bouton">
                <form action="index.php?action=setNbArticlesParPage" method="POST">
                    <?php
                        echo '<input type="number" class="buttonClick" name="nbArticle" value="'.$nbArticlePages.'"/>';
                    ?>
                    <input type="submit" value="set">
                </form>
            </div>
            <div class="bouton">
                <form action="index.php">
                    <input type="hidden" name="action" value="quitterAdmin">
                    <input type="submit" class="buttonClick" value="quitter">
                </form>
            </div>
        </div>
    </div>
</html>